#!../bin/python



import json
from pymongo.errors import DuplicateKeyError
from bson.json_util import ObjectId

from core   import db
from bottle import route, run, template, redirect,response
from bottle import static_file, request, post,get


def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        return fn(*args, **kwargs)

    return _enable_cors


@route('/')
def mainpage():
    return template('templates/mainp.html')

@route('/hello/<esm>')
def greet(esm='Stranger'):
    f = open('data.txt', 'a')
    f.write('name: {}\n'.format(esm))
    f.close()
    return template('Hello <strong>{{e}}</strong>, how are you?', e=esm)
@route('/hello/')
def hello():
    k='Fatima'
    return template('Hello world Python<b> {{p}}</b>',p=k)

@route('/read_database/<_pass>')
def read(_pass):
    data = "Wrong Pass"
    if _pass == '12345':
        f = open('data.txt', 'r')
        data = f.read()
        f.close()

@get('/webservice/test_get')
@enable_cors
def service_get():
    return "salam get"


@post('/webservice/test_post')
@enable_cors
def service_post():
    data = request.json
    data=json.dumps(data)
    print(data)
    return data
    
    
    return template(" content of file data.txt equal : </br>{{data}}", data=data)


@route('/statics/css/<filename:re:.*\.css>')
def send_css(filename):
    return static_file(filename, root='statics/css')

@route('/statics/js/<filename:re:.*\.js>')
def send_js(filename):
    return static_file(filename, root='statics/js')



"""@route('/form/<form_name>')
def form_view(form_name):
    return template('templates/' + form_name)"""
@get('/form/<form_name>')
def form_view(form_name):
    data={}
    return template('templates/' + form_name,data=data)

@route('/statics/<filename>')
def server_static(filename):
    return static_file(filename, root='statics/')



@post('/form-submit')
def form_submit():
    f = request.forms.get('firstname', None)
    l = request.forms.get('lastname', None)
    c = request.forms.get('country', None)
    
    u = request.forms.get('username', None)
    p = request.forms.get('password', None)
    z = request.forms.get('zipcod', None)
    r = request.forms.get('confpassword', None)
    m= request.forms.get('mobileno', None)
    e = request.forms.get('email', None)
    if (r==p):
	account_info=dict(
        firstname=request.forms.get('firstname', None),
        lastname= request.forms.get('lastname', None),
	country = request.forms.get('country', None),
    	username = request.forms.get('username', None),
    	password = request.forms.get('password', None),
    	zipcode = request.forms.get('zipcod', None),
    	mobile_number= request.forms.get('mobileno', None),
    	email = request.forms.get('email', None)
    	)
	db.Usertbl.insert_one(account_info)
        return redirect("/")
    else:
	data={}
	data['error']='error,Your Password is not Equal'
        return template('templates/register.html')

@post('/webservice/login')
@enable_cors
def ws_form_login():
    f=''
    data = request.json
    print(data)
    """d=json.dumps(data)
    print(d)
    d1=json.loads(d)
    print(d1)"""
    f = data['UserName']
    l = data['password']
    isuser=False   
    isuser=db.Usertbl.find_one({'username':unicode(f),'password':unicode(l)})
    print('isuser=',isuser)
    print('f=',f)
    print('l=',l)
    """isuser=True"""
    if isuser:
	    return json.dumps(True) 
    else:
            return json.dumps(False)          




@post('/login')
def form_login():
    f = request.forms.get('UserName', None)
    l = request.forms.get('password', None)
    """isuser=False"""
    isuser=db.Usertbl.find_one({'username':f,'password':l})
    print(isuser)
    print(f)
    print(l)
    if isuser:
	    response.set_cookie('user_id',str(isuser['_id']), path='/')
            return redirect("/") 
    else:
            return redirect("templates/login.html")           
        
@get('/users/logout')
def logout():
    response.delete_cookie('user_id', path='/')
    redirect('/')





@route('/successful')
def successful():
    data = list(db.Usertbl.find().limit(20))
    
    return template('templates/table.html', data=data)
    

if __name__ == '__main__':
    run(host='0.0.0.0', port=8181, debug=True)
